/**
 * View Models used by Spring MVC REST controllers.
 */
package com.sp.flow.web.rest.vm;
